module something

union status = {
  😡 = {errorMessage}
  🙂 = {successValue}
  🥶
}

augment status$😡 {
  function 🚀 = |this, 🔴, 🟢| -> 🔴(this: errorMessage())
}

augment status$🙂 {
  function 🚀 = |this, 🔴, 🟢| -> 🟢(this: successValue())
}

function divide = |a, b| {
  try {
    return status.🙂(a/b)
  } catch (error) {
    return status.😡(error: getMessage())
  }
}

function main = |args| {

  let res = (
    |it| -> match {
      when it: is😡() then it: errorMessage()
      when it: is🙂() then it: successValue()
      otherwise status.🥶()
    }
  ) (divide(10,0))

  println(res)

  # `then` is like a `return`, so `then {foo}` returns a FunctionReference
  (|it| -> match {
      when it: is😡() then {
        println("Ouch: " + it: errorMessage())
      }
      when it: is🙂() then {
        println("Yeah: " + it: successValue())
      }
      otherwise status.🥶()
    }
  ) (divide(10.0,3.0)) () # calling the FunctionReference

  divide(10.0,3.0): 🚀(
    🔴=|error| -> println(error),
    🟢=|value| -> println(value)
  )

}
