module something

union status = {
  😡 = {errorMessage}
  🙂 = {successValue}
  🥶
}

augment status$😡 {
  function 🚀 = |this, 💥, 🎉| -> 💥(this: errorMessage())
}

augment status$🙂 {
  function 🚀 = |this, 💥, 🎉| -> 🎉(this: successValue())
}

function divide = |a, b| {
  try {
    return status.🙂(a/b)
  } catch (error) {
    return status.😡(error: getMessage())
  }
}

function main = |args| {

  divide(10,0): 🚀(
    💥=|error| -> println(error),
    🎉=|value| -> println(value)
  )

}
