FROM ubuntu:latest AS builder
LABEL maintainer="@k33g_org"
 
RUN apt-get update && \
    apt-get install -y apt-utils && \
    apt-get install -y curl && \
    apt-get install -y nano && \
    apt-get install -y gnupg2 && \
    apt install -y openjdk-8-jdk && \
    apt install -y maven && \
    apt install -y mercurial && \
    apt install -y git && \
    apt-get -y install locales  && \
    apt-get clean

# Set the locale
RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8  

# Install Golo
RUN cd home && \
    git clone https://github.com/eclipse/golo-lang.git && \
    cd golo-lang && \
    ./gradlew installDist

FROM gitpod/workspace-full

COPY --from=builder /home/golo-lang/ /golo-lang/.

RUN echo "export GOLO_HOME=/golo-lang/build/install/golo" >> ~/.bashrc  && \
    echo "export PATH=\$PATH:\$GOLO_HOME/bin" >> ~/.bashrc
