module hello

# How to run it:
# golo golo --files hello.golo

union status = {
  OK
  FAILED
}

function main = |args| {
  println("😃 Hello Golo")
  let a = status.OK()
  println(a)
}
